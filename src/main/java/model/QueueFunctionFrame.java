package model;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class QueueFunctionFrame extends JFrame {
	private static final long serialVersionUID = 1L;

	// LABELS
	JLabel numberOfQueuesLabel = new JLabel("Number Of Queues:");
	JLabel simulationIntervalLabel = new JLabel("Simulation Interval:");
	JLabel minArrivingTimeLabel = new JLabel("Min Arriving Time:");
	JLabel maxArrivingTimeLabel = new JLabel("Max Arriving Time:");
	JLabel minServiceTimeLabel = new JLabel("Min Service Time:");
	JLabel maxServiceTimeLabel = new JLabel("Max Service Time:");
	JLabel peackWaitingTimeLabel = new JLabel("Peack Waiting Time:");
	JLabel averageWaitingTImeLabel = new JLabel("Average Waiting Time:");
	JLabel simulationLabel = new JLabel("Simulation");

	// BUTTONS

	JButton startButton = new JButton("Start"); // Creat all the buttons, textfields and labels you need

	// TEXT FIELDS
	JTextField numberOfQueuesTextField = new JTextField();
	JTextField simulationIntervalTextField = new JTextField();
	JTextField minArrivingTimeTextField = new JTextField();
	JTextField maxArrivingTimeTextField = new JTextField();
	JTextField minServiceTimeTextField = new JTextField();
	JTextField maxServiceTimeTextField = new JTextField();
	JTextField peackWaitingTimeTextField = new JTextField();
	JTextField averageWaitingTImeTextField = new JTextField();
	// JTextField simulationTextField = new JTextField();

	final JPanel panel = new JPanel();

	public QueueFunctionFrame() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
			UnsupportedLookAndFeelException {

		UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");

		setSize(700, 550); // size of the panel
		setTitle("Queues"); // title
		setResizable(false); // not allows the user to extend the window

		// create panel

		panel.setLayout(null);
		panel.add(numberOfQueuesLabel);
		panel.add(simulationIntervalLabel);
		panel.add(minArrivingTimeLabel);
		panel.add(maxArrivingTimeLabel);
		panel.add(maxServiceTimeLabel);
		panel.add(minServiceTimeLabel);
		panel.add(peackWaitingTimeLabel);
		panel.add(averageWaitingTImeLabel);
		panel.add(simulationLabel);

		panel.add(startButton);

		panel.add(numberOfQueuesTextField);
		panel.add(simulationIntervalTextField); // add everithing to the panel
		panel.add(minArrivingTimeTextField);
		panel.add(maxArrivingTimeTextField);
		panel.add(minServiceTimeTextField);
		panel.add(maxServiceTimeTextField);
		panel.add(peackWaitingTimeTextField);
		panel.add(averageWaitingTImeTextField);

		// LEFT SIZE PANNEL
		numberOfQueuesLabel.setBounds(80, 10, 200, 40);
		simulationIntervalLabel.setBounds(80, 80, 200, 40);
		minArrivingTimeLabel.setBounds(80, 150, 200, 40);
		maxArrivingTimeLabel.setBounds(80, 220, 200, 40);
		minServiceTimeLabel.setBounds(80, 290, 200, 40);
		maxServiceTimeLabel.setBounds(80, 360, 200, 40);

		startButton.setBounds(120, 430, 100, 40);

		numberOfQueuesTextField.setBounds(300, 10, 40, 30);
		simulationIntervalTextField.setBounds(300, 80, 40, 30); // set bounds to all
		minArrivingTimeTextField.setBounds(300, 150, 40, 30);
		maxArrivingTimeTextField.setBounds(300, 220, 40, 30);
		minServiceTimeTextField.setBounds(300, 290, 40, 30);
		maxServiceTimeTextField.setBounds(300, 360, 40, 30);

		// RIGHT SIZE PANNEL
		simulationLabel.setBounds(485, 10, 150, 40);

		simulationLabel.setForeground(Color.BLACK);
		simulationLabel.setFont(simulationLabel.getFont().deriveFont(17.0f));
		startButton.setForeground(Color.BLACK);
		startButton.setFont(startButton.getFont().deriveFont(17.0f));
		numberOfQueuesLabel.setForeground(Color.BLACK);
		numberOfQueuesLabel.setFont(numberOfQueuesLabel.getFont().deriveFont(17.0f));
		simulationIntervalLabel.setForeground(Color.BLACK);
		simulationIntervalLabel.setFont(simulationIntervalLabel.getFont().deriveFont(17.0f));
		minArrivingTimeLabel.setForeground(Color.BLACK);
		minArrivingTimeLabel.setFont(minArrivingTimeLabel.getFont().deriveFont(17.0f));
		maxArrivingTimeLabel.setForeground(Color.BLACK);
		maxArrivingTimeLabel.setFont(maxArrivingTimeLabel.getFont().deriveFont(17.0f));
		minServiceTimeLabel.setForeground(Color.BLACK);
		minServiceTimeLabel.setFont(minServiceTimeLabel.getFont().deriveFont(17.0f));
		maxServiceTimeLabel.setForeground(Color.BLACK);
		maxServiceTimeLabel.setFont(maxServiceTimeLabel.getFont().deriveFont(17.0f));

		startButton.addActionListener(new ActionListener() { // add action to button Start
			public void actionPerformed(ActionEvent e) {
				verify(numberOfQueuesTextField, simulationIntervalTextField, minArrivingTimeTextField,
						maxArrivingTimeTextField, minServiceTimeTextField, maxServiceTimeTextField);
			}
		});

		this.setContentPane(panel);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public void verify(JTextField numberOfQueues, JTextField simulationInterval, JTextField minArrivingTime,		
			JTextField maxArrivingTime, JTextField minServiceTime, JTextField maxServiceTime) { 				//method verifying weather the input the user provided is a valid one or an invalid one.
		boolean numbersOk = true;
		String numberOfGivenQueues = numberOfQueues.getText();					
		String simulationTimeGiven = simulationInterval.getText();
		String minGivenArrivingTime = minArrivingTime.getText();
		String maxGivenArrivingTime = maxArrivingTime.getText();
		String minGivenServcieTime = minServiceTime.getText();
		String maxGivenServiceTime = maxServiceTime.getText();
		if (numberOfGivenQueues.equals("") && simulationTimeGiven.equals("") && minGivenArrivingTime.equals("")
				&& maxGivenArrivingTime.equals("") && minGivenServcieTime.equals("")
				&& maxGivenServiceTime.equals("")) {
			JOptionPane.showMessageDialog(this, "Error. Please complete all six fields!", "Error",
					JOptionPane.WARNING_MESSAGE);
		} else {

			int verifiedNumberOfGivenQueues = getIntFromString(numberOfGivenQueues);
			if (verifiedNumberOfGivenQueues == 0) {
				JOptionPane.showMessageDialog(this, "Error. Add at least one queue!", "Error",
						JOptionPane.WARNING_MESSAGE);
				numbersOk &= false;
			}
			int verifiedSimulationTimeGiven = getIntFromString(simulationTimeGiven);
			if (verifiedSimulationTimeGiven < 1) {
				JOptionPane.showMessageDialog(this, "Error. Enter a positive simulation time!", "Error",
						JOptionPane.WARNING_MESSAGE);
				numbersOk &= false;
			}
			int verifiedMinGivenArrivingTime = getIntFromString(minGivenArrivingTime);
			int verifiedMaxGivenArrivingTime = getIntFromString(maxGivenArrivingTime);
			if (verifiedMinGivenArrivingTime > verifiedMaxGivenArrivingTime) {
				JOptionPane.showMessageDialog(this, "Error. Min Arriving time bigger than Max Arriving time!", "Error",
						JOptionPane.WARNING_MESSAGE);
				numbersOk &= false;
			}

			int verifiedMinGivenServiceTime = getIntFromString(minGivenServcieTime);
			int verifiedMaxGivenServiceTime = getIntFromString(maxGivenServiceTime);
			if (verifiedMinGivenServiceTime > verifiedMaxGivenServiceTime) {
				JOptionPane.showMessageDialog(this, "Error. Min Service time bigger than Max Service time!", "Error",
						JOptionPane.WARNING_MESSAGE);
				numbersOk &= false;
			}
			if (numbersOk) {
				int yCoordonate = 80;
				ServingQueue queue[] = new ServingQueue[verifiedNumberOfGivenQueues];
				for (int i = 0; i < verifiedNumberOfGivenQueues; i++) {
					JTextField queuePrinterTextField = new JTextField();
					queuePrinterTextField.setBounds(400, yCoordonate, 200, 30);
					yCoordonate += 80;
					panel.add(queuePrinterTextField);
					String queueName = "Queue " + i;
					JTextFieldCurrentActionPrinter printer = new JTextFieldCurrentActionPrinter(queuePrinterTextField,
							queueName);
					queue[i] = new ServingQueue(queueName);
					queue[i].setPrinter(printer);
					queue[i].start();

				}
				QueueManager queueManager = new QueueManager(verifiedNumberOfGivenQueues, verifiedSimulationTimeGiven,
						verifiedMinGivenArrivingTime, verifiedMaxGivenArrivingTime, verifiedMinGivenServiceTime,
						verifiedMaxGivenServiceTime, queue);
				queueManager.start();
			}
		}
	}

	private Integer getIntFromString(String valueToCast) {
		Integer result = null;
		try {
			result = Integer.valueOf(valueToCast);
		} catch (NumberFormatException e) {
		}
		return result;
	}

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException,
			IllegalAccessException, UnsupportedLookAndFeelException, FileNotFoundException {
		File file = new File("LogOfEvenets_Peak_Wait.txt");
		FileOutputStream fos = new FileOutputStream(file);
		PrintStream ps = new PrintStream(fos);
		System.setOut(ps);
		QueueFunctionFrame aux = new QueueFunctionFrame();
		aux.setVisible(true);
	}

}
