package model;

public class Client {

	private String name;
	private int servingTime;
	private int arrivingTime; // client attributes

	public Client(String name, int servinTime, int arrivingTime) { //Client constructor
		this.name = name;
		this.servingTime = servinTime;
		this.arrivingTime = arrivingTime;
	}

	public int getArrivingTime() {
		return arrivingTime;
	}

	public void setArrivingTime(int arrivingTime) {
		this.arrivingTime = arrivingTime;
	}

	public String getName() {
		return name;
	}
															//Getters and Setters for all the attributes of the client class
	public void setName(String name) {
		this.name = name;
	}

	public int getServingTime() {
		return servingTime;
	}

	public void setServingTime(int servingTime) {
		this.servingTime = servingTime;
	}

	@Override
	public String toString() {  //override of toString method
		return this.name + " ";
	}

}
