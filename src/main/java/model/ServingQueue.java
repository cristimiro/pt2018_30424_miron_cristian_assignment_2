package model;

import java.util.ArrayList;
import java.util.Iterator;

public class ServingQueue extends Thread {

	private String queueName;
	private ArrayList<Client> clientList = new ArrayList<Client>();			// attributes of the ServingQueue
	private JTextFieldCurrentActionPrinter printer;

	public ServingQueue(String queueName) {
		this.queueName = queueName; //constructor of the ServingQueue
	}
	//getter for clientList
	public ArrayList<Client> getClientList() {
		return clientList;
	}

	public synchronized void addClient(Client client) {
		this.clientList.add(client);					//method which adds a client to the list
		this.printer.setCurrentAction(print());	
	}

	public synchronized void removeClient(Client client) {
		this.clientList.remove(client);					//method which removes a client to the list
		this.printer.setCurrentAction(print());
	}

	public synchronized int listSize() {
		return this.clientList.size();				//method returning the number of clients a list has;

	}

	public void run() {								//override to run method from Thread class
		while (true) {
			if (listSize() != 0) {
				Iterator<Client> it = this.clientList.iterator();
				Client aux = it.next();								// takes each client one by one, and after their serving time has passed, it removes the client from the list
				try {
					Thread.sleep(aux.getServingTime() * 1000);
					System.out.println(aux.getName() + " left the queue " + this.queueName);
					removeClient(aux);
				} catch (InterruptedException e) {
					e.printStackTrace();
					System.out.println("Error");
				}
			}
		}
	}

	private String print() {			//prints the client list for the real-time simulation
		String message = "";
		for (Client client : this.clientList) {
			message += client.toString();
		}
		return message;
	}

	public void setPrinter(JTextFieldCurrentActionPrinter printer) {	//sets the printer attribute
		this.printer = printer;
	}

}
