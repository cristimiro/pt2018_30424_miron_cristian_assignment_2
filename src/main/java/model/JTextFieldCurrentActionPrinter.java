package model;

import javax.swing.JTextField;

public class JTextFieldCurrentActionPrinter {

	private JTextField txtField; //attributes of the JTextFieldCurrentActionPrinter
	private String name;

	public JTextFieldCurrentActionPrinter(JTextField txtField, String name) {
		this.txtField = txtField;
		this.txtField.setText(name); //constructor of the JTextFieldCurrentActionPrinter
		this.name = name;
	}

	public synchronized void setCurrentAction(String message) {
		this.txtField.setText(this.name + " " + message);		//real-time queue evolution printer
	}

}
