package model;
import java.util.Random;

public class QueueManager extends Thread {
	private int numberOfQueues;
	private volatile int simulationTime;
	private int minArrivingTime;
	private int maxArrivingTime;
	private int minServingTime;						//attributes of the QueueManager
	private int maxServingTime;
	private int numberOfQueuesAtPeakTime;
	private int peakTime;
	private ServingQueue queue[] ;
	
	public QueueManager(int numberOfQueues, int simulationTime, int minArrivingTime,int maxArrivingTime, int minServingTime, int maxServingTime,ServingQueue[] servQueue) {
		this.numberOfQueues = numberOfQueues;
		this.simulationTime = simulationTime;
		this.minArrivingTime = minArrivingTime;
		this.maxArrivingTime = maxArrivingTime;
		this.minServingTime = minServingTime;
		this.maxServingTime = maxServingTime;						//constructor of the QueueManager
		this.queue = new ServingQueue[numberOfQueues];
		for (int i=0;i<numberOfQueues;i++) {
			this.queue[i] =servQueue[i];
		}
		
	}
	
	public int randomArrivingTime() {
		Random random = new Random();
		return random.nextInt(this.maxArrivingTime-this.minArrivingTime) + this.minArrivingTime;
	}
																	//methods generating random arriving and serving time for clients.
	public int randomServingTime() {
		Random random = new Random();
		return random.nextInt(this.maxServingTime-this.minServingTime) + this.minServingTime;
	}

	
	public int getPeakTime() {
		return peakTime;
	}
											//Getter and setter for the PeakTime
	public void setPeakTime(int numberOfQueuesAtPeakTime,int peackTime) {
		if (this.numberOfQueuesAtPeakTime<numberOfQueuesAtPeakTime) {
			this.numberOfQueuesAtPeakTime = numberOfQueuesAtPeakTime;
			this.peakTime = peackTime;
		}		
	}

	public void run() {							//override to run method from Thread class
		int clientNumber = 1,initialTime= 0;
		double waitingTime=0;
		while (this.simulationTime > 0 ) {
			int emptiestQueue=0, numberOfQueuesAtPeakTime=0,maxNumberToCompareWith = Integer.MAX_VALUE;
			for (int i=0;i<this.numberOfQueues;i++) {
				if (maxNumberToCompareWith > this.queue[i].listSize()) {
					emptiestQueue = i;										//verifies the queue which has the minimum amount of clients.
					maxNumberToCompareWith = this.queue[i].listSize();}
				numberOfQueuesAtPeakTime += this.queue[i].listSize();
			}
			setPeakTime(numberOfQueuesAtPeakTime,initialTime); //peak time
			Client customer = new Client("Customer "+clientNumber, randomServingTime(),randomArrivingTime());	// creates a new client and then adds the client to the list with the minimum amount of clients;
			System.out.println("Customer "+clientNumber+" Arrived at Queue "+emptiestQueue+" after "+initialTime +" since SimStart. Serving Time: "+customer.getServingTime() + " arrTime "+customer.getArrivingTime());
			queue[emptiestQueue].addClient(customer);
			initialTime+=customer.getArrivingTime();
			waitingTime+=customer.getServingTime();		//tracks the time the clients are waiting for what they bought
			try {
				Thread.sleep(customer.getArrivingTime()*1000);
			} catch (InterruptedException e) {
				e.printStackTrace();}
			if (this.simulationTime - customer.getArrivingTime() >0 ) {
				clientNumber++;
				setSimulationTime(this.simulationTime - customer.getArrivingTime());	// subtracts the initial simulation time until it is less than 0, than the thread stops.
			}else {
				System.out.println("Average waiting time was: "+waitingTime/clientNumber+" and peakTime was "+this.peakTime);
				System.out.println("FINISH");
				break;}
		}
	}
	//getter and setter for the simulation time;
	public int getSimulationTime() {
		return simulationTime;
	}
	public void setSimulationTime(int simulationTime) {
		this.simulationTime = simulationTime;
	}
}
